var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');
var handleLexResponse = require('../helpers/handleLexResponse');

module.exports = {
  validateVehicleSearch: function() {
    return helpermethods.buildValidationResult(true, null, null);
  },

  dovehiclesearch: function(intentRequest, callback) {
    const source = intentRequest.invocationSource;
    if (source === 'DialogCodeHook') {
      // Perform basic validation on the supplied input slots.  Use the elicitSlot dialog action to re-prompt for the first violation detected.
      //if(intentRequest.sessionAttributes
      const slots = intentRequest.currentIntent.slots;
      const validationResult = this.validateVehicleSearch();
      if (!validationResult.isValid) {
        slots[`${validationResult.violatedSlot}`] = null;
        callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, validationResult.violatedSlot, validationResult.message));
        return;
      }

      const outputSessionAttributes = intentRequest.sessionAttributes || {};

      // outputSessionAttributes.id = id;
      // outputSessionAttributes.tag = tag;

      callback(helpermethods.delegate(outputSessionAttributes, intentRequest.currentIntent.slots));
      return;
    }
    const yesorno = intentRequest.currentIntent.slots.yesorno;
    console.log("YesorNo Value is:" + yesorno)
    outputSessionAttributes = intentRequest.sessionAttributes || {};


    console.log("Session attributes");
    if (yesorno == undefined) {
      handleLexResponse.handleVehicleResponseSession(intentRequest, callback);
    } else if (yesorno == 'yes') {
      handleLexResponse.handleTextResponsehtml(intentRequest, callback, "yesorno", "Please specify html");
    } else {
      var totalSales = 0;
      var endpoint = 'https://webdesking-staging.tagrail.com/api/v1/inventory/9687/filterinventory';
      request.post(
        endpoint, {
          json: {
            pageNumber: 1,
            pageSize: 10,
            bodyStyle: null,
            paymentType: 0,
            payment: {
              min: 0,
              max: 2000
            },
            make: "",
            model: "",
            vehicleType: "1"
          }
        },
        function(error, response, body) {
          if (!error && response.statusCode == 200) {
            var result = body.result;
            var totalNo = result.length;
            console.log(result);
            // callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
            //   contentType: 'PlainText',
            //   content: stringResult
            // }));
            handleLexResponse.handleVehicleResponse(intentRequest, callback, result);
          } else {
            callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
              contentType: 'PlainText',
              content: `Sorry Failed to Vehicle details.`
            }));
          }
        }
      );
    }
  }
}
