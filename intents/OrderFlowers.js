var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');

module.exports = {
  validateOrderFlowers: function (flowerType, date, time) {
    const flowerTypes = ['lilies', 'roses', 'tulips'];
    if (flowerType && flowerTypes.indexOf(flowerType.toLowerCase()) === -1) {
      return helpermethods.buildValidationResult(false, 'FlowerType', `We do not have ${flowerType}, would you like a different type of flower?  Our most popular flowers are roses`);
    }
    if (date) {
      console.log("date:"+date);
      console.log(utilmethods);
      if (!utilmethods.isValidDate(date)) {
        console.log("date is not valid");
        console.log(helpermethods);
        console.log(this);
        return helpermethods.buildValidationResult(false, 'PickupDate', 'I did not understand that, what date would you like to pick the flowers up?');
      }
      if (utilmethods.parseLocalDate(date) < new Date()) {
        return lexmethods.buildValidationResult(false, 'PickupDate', 'You can pick up the flowers from tomorrow onwards.  What day would you like to pick them up?');
      }
    }
    if (time) {
      if (time.length !== 5) {
        // Not a valid time; use a prompt defined on the build-time model.
        return helpermethods.buildValidationResult(false, 'PickupTime', null);
      }
      const hour = parseInt(time.substring(0, 2), 10);
      const minute = parseInt(time.substring(3), 10);
      if (isNaN(hour) || isNaN(minute)) {
        // Not a valid time; use a prompt defined on the build-time model.
        return helpermethods.buildValidationResult(false, 'PickupTime', null);
      }
      if (hour < 10 || hour > 16) {
        // Outside of business hours
        return helpermethods.buildValidationResult(false, 'PickupTime', 'Our business hours are from ten a m. to five p m. Can you specify a time during this range?');
      }
    }
    return helpermethods.buildValidationResult(true, null, null);
  },

  orderFlowers: function (intentRequest, callback) {
    const flowerType = intentRequest.currentIntent.slots.FlowerType;
    const date = intentRequest.currentIntent.slots.PickupDate;
    const time = intentRequest.currentIntent.slots.PickupTime;
    const source = intentRequest.invocationSource;

    if (source === 'DialogCodeHook') {
      // Perform basic validation on the supplied input slots.  Use the elicitSlot dialog action to re-prompt for the first violation detected.
      const slots = intentRequest.currentIntent.slots;
      const validationResult = this.validateOrderFlowers(flowerType, date, time);
      if (!validationResult.isValid) {
        slots[`${validationResult.violatedSlot}`] = null;
        callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, validationResult.violatedSlot, validationResult.message));
        return;
      }

      // Pass the price of the flowers back through session attributes to be used in various prompts defined on the bot model.
      const outputSessionAttributes = intentRequest.sessionAttributes || {};
      if (flowerType) {
        outputSessionAttributes.Price = flowerType.length * 5; // Elegant pricing model
      }
      callback(helpermethods.delegate(outputSessionAttributes, intentRequest.currentIntent.slots));
      return;
    }

    // Order the flowers, and rely on the goodbye message of the bot to define the message to the end user.  In a real bot, this would likely involve a call to a backend service.
    callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
      contentType: 'PlainText',
      content: `Thanks, your order for ${flowerType} has been placed and will be ready for pickup by ${time} on ${date}`
    }));
  }
}
