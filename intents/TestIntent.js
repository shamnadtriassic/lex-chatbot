var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');

module.exports = {
  testmethod: function(intentRequest, callback) {
    // Order the flowers, and rely on the goodbye message of the bot to define the message to the end user.  In a real bot, this would likely involve a call to a backend service.

    var jsonResponse = "{}";
    // request
    //   .get('https://jsonplaceholder.typicode.com/posts')
    //   .on('response', function(response) {
    //     console.log(response.statusCode) // 200
    //     console.log(response.headers['content-type']) // 'image/png'
    //     console.log(response)
    //     jsonResponse = response.body;
    //     callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
    //       contentType: 'PlainText',
    //       content: `The response is ${jsonResponse}`
    //     }));
    //   })
    //   .on('error', function(err) {
    //     console.log(err);
    //     callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
    //       contentType: 'PlainText',
    //       content: `Exception occured ${err}`
    //     }));
    //   })
      var endpoint = 'https://jsonplaceholder.typicode.com/posts/1';
        request.get(
          endpoint, {
          },
          function(error, response, body) {
            if (!error && response.statusCode == 200) {
              console.log(body);
              jsonResponse = body;
              callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
                contentType: "PlainText",
                content: jsonResponse
              }));
            } else {
              console.log(error);
              callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
                contentType: 'PlainText',
                content: `Exception occured ${error}`
              }));
            }
          }
        );
  }
}
