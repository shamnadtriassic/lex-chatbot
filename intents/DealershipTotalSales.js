var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');

module.exports = {
  validateTotalSales: function (date) {
    console.log(date);
    return helpermethods.buildValidationResult(true, null, null);
  },

  dealershipsales: function (intentRequest, callback) {
    const date = intentRequest.currentIntent.slots.forDate;
    const source = intentRequest.invocationSource;
    console.log("source:"+source);
    if (source === 'DialogCodeHook') {
      // Perform basic validation on the supplied input slots.  Use the elicitSlot dialog action to re-prompt for the first violation detected.
      const slots = intentRequest.currentIntent.slots;
      const validationResult = this.validateTotalSales(date);
      if (!validationResult.isValid) {
        slots[`${validationResult.violatedSlot}`] = null;
        callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, validationResult.violatedSlot, validationResult.message));
        return;
      }

      const outputSessionAttributes = intentRequest.sessionAttributes || {};

        // outputSessionAttributes.id = id;
        // outputSessionAttributes.tag = tag;

      callback(helpermethods.delegate(outputSessionAttributes, intentRequest.currentIntent.slots));
      return;
    }
    var totalSales = 0;
    var endpoint = 'http://tagrail-demo.cloudapp.net/trial/LexusDemo/api/v1/usagereport/GenerateMonthlyReport';
      request.post(
        endpoint, {
          json: {}
        },
        function(error, response, body) {
          if (!error && response.statusCode == 200) {
            totalSales = body.result.totalSales;
            console.log(body.result);
            console.log("totalSales:"+totalSales);
          } else {
            console.log("totalSales: coult not be found");
          }
        }
      );
    // Order the flowers, and rely on the goodbye message of the bot to define the message to the end user.  In a real bot, this would likely involve a call to a backend service.
    callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
      contentType: 'PlainText',
      content: `Your dealership Total sales for the month is ${totalSales}`
    }));
  }
}
