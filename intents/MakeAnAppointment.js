var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');
var handleLexResponse = require('../helpers/handleLexResponse');

module.exports = {
  validateAppointment: function(date) {
    console.log(date);
    return helpermethods.buildValidationResult(true, null, null);
  },

  makeAnAppointment: function(intentRequest, callback) {
    // const id = intentRequest.currentIntent.slots.id;
    // const tag = intentRequest.currentIntent.slots.tag;
    const date = intentRequest.currentIntent.slots.forDate;
    const email = intentRequest.currentIntent.slots.email;
    const phone = intentRequest.currentIntent.slots.phone;
    const name = intentRequest.currentIntent.slots.name;
    const when = intentRequest.currentIntent.slots.when;
    const confirm = intentRequest.currentIntent.slots.confirm;
    const source = intentRequest.invocationSource;
    console.log("Source:"+source);
    if (name == undefined) {
      handleLexResponse.handleTextResponseWithSlot(intentRequest, callback, "name", "Please specify your name");

    } else if (email == undefined) {
      handleLexResponse.handleTextResponseWithSlot(intentRequest, callback, "email", "Please specify your emaild id");
    } else if (phone == undefined) {
      handleLexResponse.handleTextResponseWithSlot(intentRequest, callback, "phone", "Please specify your phone number");
    } else if (date == undefined) {
      handleLexResponse.handleTextResponseWithSlot(intentRequest, callback, "forDate", "Please specify the date?");
    } else if (when == undefined) {
      handleLexResponse.handleTextResponseWithSlot(intentRequest, callback, "when", "Please specify the time");
    } else if (confirm == undefined) {
      handleLexResponse.handleTextResponseWithAction(intentRequest, callback, "confirm", "Do you want to make the appointment on "+date+" at "+when, ["Yes","No"]);
    }

    if(confirm == 'No' && when != undefined) {
      handleLexResponse.handleTextResponseWithSlot(intentRequest, callback, "forDate", "Please specify the date?");
      intentRequest.currentIntent.slots.when = undefined;
    }
    console.log("source:" + source);
    if (source === 'DialogCodeHook') {
      // Perform basic validation on the supplied input slots.  Use the elicitSlot dialog action to re-prompt for the first violation detected.
      const slots = intentRequest.currentIntent.slots;
      const validationResult = this.validateAppointment(date);
      if (!validationResult.isValid) {
        slots[`${validationResult.violatedSlot}`] = null;
        callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, validationResult.violatedSlot, validationResult.message));
        return;
      }

      const outputSessionAttributes = intentRequest.sessionAttributes || {};

      // outputSessionAttributes.id = id;
      // outputSessionAttributes.tag = tag;

      callback(helpermethods.delegate(outputSessionAttributes, intentRequest.currentIntent.slots));
      return;
    }
    // callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
    //   contentType: 'PlainText',
    //   content: `Please wait while I try to add your appointment to the system`
    // }));
    this.AddAppointmentAPI(intentRequest, callback);
  },

  'AddAppointmentAPI': function(intentRequest, callback) {
    var endpoint = 'http://tagrail-demo.cloudapp.net/trial/LexusDemo/api/v1/report/getLeadsTracker';
    var intentObj = intentRequest.currentIntent
    var thisObj = this;
    console.log(intentObj.slots.name);
    console.log(intentObj.slots.forDate);
    console.log(intentObj.slots.when);
    var name = intentObj.slots.name;
    var date = intentObj.slots.forDate;
    var when = intentObj.slots.when;
    console.log("when is :" + when);
    console.log("name is :" + name);
    console.log("date is :" + date);
    var leadid = -1;
    request.post(
      endpoint, {
        json: {}
      },
      function(error, response, body) {
        if (!error && response.statusCode == 200) {
          var results = body.result;
          var totalNo = results.length;

          for (var i = 0; i < totalNo; i++) {
            var firstname = results[i].first_name.toUpperCase();
            if ((firstname != null) && (firstname != undefined)) {
              var compare = name.toUpperCase();
              if (firstname == compare) {
                leadid = results[i].lead_id;
                break;
              }
            }
          }
          if (leadid == -1) {
            var date = intentObj.slots.forDate;
            var when = intentObj.slots.when;
            var phone = "123456789";
            var email = "lex@mithra.com";
            var userid = "110";
            thisObj.AddingNewLeadAPI(name, phone, email, userid, date, when, intentRequest, callback);
          } else {
            var date = intentObj.slots.forDate;
            var when = intentObj.slots.when;
            var userid = "110";
            thisObj.AddAppointmentAPICall(name, date, when, userid, leadid, intentRequest, callback);
          }
        } else {
          //Error
          // callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
          //   contentType: 'PlainText',
          //   content: `Sorry ${name}, Failed to add an appointment for you`
          // }));
          handleLexResponse.handleTextResponse(intentRequest, callback, `Sorry ${name}, Failed to add an appointment for you`);
        }
      }
    );
  },

  'AddAppointmentAPICall': function(name, date, when, userid, leadid, intentRequest, callback) {
    var thisObj = this;
    var startdate = helpermethods.GetStartDateValue(date);
    console.log(startdate);
    var datesplit = startdate.split("/");
    var actualdate = datesplit[2] + "-" + datesplit[1] + "-" + datesplit[0];
    var time = helpermethods.GetStringTimeValue(when);
    console.log("Time converted:" + time);
    actualdate = actualdate + " " + when + ":00";
    console.log("Add appointment");
    console.log("name:" + name);
    console.log("leadid:" + leadid);
    console.log("userid:" + userid);
    console.log("appointmentdate:" + actualdate);
    endpoint = 'http://tagrail-demo.cloudapp.net/trial/LexusDemo/api/ups/apntmntadd';
    request.post(
      endpoint, {
        json: {
          user_id: userid,
          appointment_date: actualdate,
          contact_id: "0",
          created_by: userid,
          lead_id: leadid
        }
      },
      function(error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log("AddAppointmentAPI is:" + body.result);
          // callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
          //   contentType: 'PlainText',
          //   content: `Hi ${name}, added new Appointment  on ${startdate} at ${time}`
          // }));
          handleLexResponse.handleTextResponse(intentRequest, callback, `Hi ${name}, added new Appointment  on ${startdate} at ${time}`);
        } else {
          console.log("Add new appointmnet error is:" + error);
          // callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
          //   contentType: 'PlainText',
          //   content: `Sorry ${name}, Failed to add an appointment for you`
          // }));
          handleLexResponse.handleTextResponse(intentRequest, callback , `Sorry ${name}, Failed to add an appointment for you`);
        }
      }
    );
  },

  'AddingNewLeadAPI': function(name, phone, email, userid, date, when, intentRequest, callback) {
    var thisObj = this;
    var endpoint = 'http://tagrail-demo.cloudapp.net/trial/LexusDemo/api/v1/report/updateLeadInfo';
    var splitnames = name.split(" ");
    var first = splitnames[0];
    var last = splitnames[1];
    console.log("first:" + first);
    console.log("last" + last);
    request.post(
      endpoint, {
        json: {
          guest_first_name: first,
          guest_last_name: last,
          cell_phone: phone,
          source: "Sales",
          primary_email: email,
          sales_person: userid
        }
      },
      function(error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log("Add new lead result is:" + body);
          thisObj.AddAppointmentAPI(intentRequest, callback);
        } else {
          callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
            contentType: 'PlainText',
            content: `Sorry ${name}, Failed to add an appointment for you`
          }));
        }
      }
    );
  },
}
