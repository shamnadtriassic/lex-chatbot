var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');
var handleLexResponse = require('../helpers/handleLexResponse');

module.exports = {
  welcomemessage: function (intentRequest, callback) {
    var message ="Hello, I am Mithra. What can I help you with?";
    handleLexResponse.handleCustomResponse(intentRequest, callback, "button-card", message);
    // callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
    //   contentType: 'PlainText',
    //   content: `Hello, I am Mithra. What can I help you with?`
    // }));


  }
}
