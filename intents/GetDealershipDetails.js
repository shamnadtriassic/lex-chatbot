var request = require('request');
var helpermethods = require('../helpers/helpermethods');
var utilmethods = require('../utils/utilmethods');
var lexmethods = require('../helpers/lexmethods');

module.exports = {
  validatedealershipdetails: function (date) {
    console.log(date);
    return helpermethods.buildValidationResult(true, null, null);
  },

  dealershipdetails: function (intentRequest, callback) {
    const date = intentRequest.currentIntent.slots.forDate;
    const source = intentRequest.invocationSource;
    console.log("source:"+source);
    if (source === 'DialogCodeHook') {
      // Perform basic validation on the supplied input slots.  Use the elicitSlot dialog action to re-prompt for the first violation detected.
      const slots = intentRequest.currentIntent.slots;
      const validationResult = this.validatedealershipdetails(date);
      if (!validationResult.isValid) {
        slots[`${validationResult.violatedSlot}`] = null;
        callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, validationResult.violatedSlot, validationResult.message));
        return;
      }

      const outputSessionAttributes = intentRequest.sessionAttributes || {};

        // outputSessionAttributes.id = id;
        // outputSessionAttributes.tag = tag;

      callback(helpermethods.delegate(outputSessionAttributes, intentRequest.currentIntent.slots));
      return;
    }
    var totalSales = 0;
    var dealer_logo = "";
    var local_address = "";
    var site_name = "";
    var working_hours = "";
    var fb_page = "";
    var doc_display_name = "";
    var endpoint = 'http://tagrail-demo.cloudapp.net/trial/LexusDemo/api/v1/app/getDealerSettings';
      request.post(
        endpoint, {
          json: {}
        },
        function(error, response, body) {
          if (!error && response.statusCode == 200) {
            dealer_logo = body.result.dealer_logo;
            local_address = body.result.local_address;
            site_name = body.result.site_name;
            working_hours = body.result.working_hours;
            doc_display_name = body.result.doc_display_name;
            console.log(body.result);
            callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
              contentType: 'PlainText',
              content: `Your dealership details are as follows ${dealer_logo} , ${doc_display_name} , ${local_address} , ${working_hours}`
            }));
          } else {
            console.log("Delearship details could not be found");
            callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
              contentType: 'PlainText',
              content: `Delearship details could not be found`
            }));
          }
        }
      );
    // Order the flowers, and rely on the goodbye message of the bot to define the message to the end user.  In a real bot, this would likely involve a call to a backend service.

  }
}
