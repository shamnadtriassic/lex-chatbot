var helpermethods = require('../helpers/helpermethods');
module.exports = {
  handleException: function(intent, error, callback) {
    callback(helpermethods.close(intent.sessionAttributes, 'Fulfilled', {
      contentType: 'PlainText',
      content: `Failed to get response due to ${error}`
    }));
  }
}
