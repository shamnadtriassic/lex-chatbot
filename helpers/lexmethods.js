var DealershipClosingRatio = require('../intents/DealershipClosingRatio');
var DealershipTotalSales = require('../intents/DealershipTotalSales');
var OrderFlowers = require('../intents/OrderFlowers');
var ExceptionHandling = require('../intents/ExceptionHandling');
var Welcome = require('../intents/Welcome');
var TestIntent = require('../intents/TestIntent');
var MakeAnAppointment = require('../intents/MakeAnAppointment');
var GetDealershipDetails = require('../intents/GetDealershipDetails');
var VehicleSearchIntent = require('../intents/VehicleSearchIntent');

module.exports = {
  dispatch: function(intentRequest, callback) {
    try {
      console.log(`dispatch userId=${intentRequest.userId}, intentName=${intentRequest.currentIntent.name}`);

      const intentName = intentRequest.currentIntent.name;
      console.log("intent is:" + intentName);
      // Dispatch to your skill's intent handlers
      if (intentName === 'closingratio') {
        return DealershipClosingRatio.closingratio(intentRequest, callback);
      } else if (intentName === 'OrderFlowers') {
        return OrderFlowers.orderFlowers(intentRequest, callback);
      } else if (intentName === 'WelcomeMessage') {
        return Welcome.welcomemessage(intentRequest, callback);
      } else if (intentName === 'DelershipSales') {
        return DealershipTotalSales.dealershipsales(intentRequest, callback);
      } else if (intentName === 'TestIntent') {
        return TestIntent.testmethod(intentRequest, callback);
      } else if (intentName === 'MakeAnAppointment') {
        return MakeAnAppointment.makeAnAppointment(intentRequest, callback);
      } else if (intentName === 'GetDealershipDetails') {
        return GetDealershipDetails.dealershipdetails(intentRequest, callback);
      } else if (intentName === 'VehicleDetailsIntent') {
        return VehicleSearchIntent.dovehiclesearch(intentRequest, callback);
      }
      throw new Error(`Intent with name ${intentName} not supported`);
    } catch (error) {
      console.log(error);
      console.log("Got in excpetion handling");
      return ExceptionHandling.handleException(intentRequest, error, callback);
    }
  }
}
