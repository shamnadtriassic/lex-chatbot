var moment = require('moment');
module.exports = {
elicitSlot: function (sessionAttributes, intentName, slots, slotToElicit, message) {
    return {
      sessionAttributes,
      dialogAction: {
        type: 'ElicitSlot',
        intentName,
        slots,
        slotToElicit,
        message,
      },
    };
  },
close: function (sessionAttributes, fulfillmentState, message) {
    return {
      sessionAttributes,
      dialogAction: {
        type: 'Close',
        fulfillmentState,
        message,
      },
    };
  },
delegate: function (sessionAttributes, slots) {
    return {
      sessionAttributes,
      dialogAction: {
        type: 'Delegate',
        slots,
      },
    };
  },
  buildValidationResult: function(isValid, violatedSlot, messageContent) {
    if (messageContent == null) {
      return {
        isValid,
        violatedSlot,
      };
    }
    return {
      isValid,
      violatedSlot,
      message: {
        contentType: 'PlainText',
        content: messageContent
      },
    };
  },

  'GetStartDateValue' : function(date) {
    console.log("inside get start date: " +date);
    var momentDate = moment(date);
    var momentDay = momentDate.date();
    var momentMonth = momentDate.month();
    var momentYear = momentDate.year();
    console.log("initial date is:"+momentDay+":"+momentMonth+":"+momentYear);
    if(momentDay < 10) {
      momentDay = "0"+momentDay;
    }
    momentMonth = momentMonth + 1;
    if(momentMonth < 10) {
      momentMonth = "0"+momentMonth;
    }

    return momentDay+"/"+momentMonth+"/"+momentYear;
  },

  'GetStringTimeValue' : function(time) {
  var respone = "04 00 PM";
  try {
  var when = time;
  var timeSplit = when.split(":");
  var hour = timeSplit[0];
  var minute = timeSplit[1];
  var timing = "AM";
  if (hour > 12) {
    hour = hour % 12;
    timing = "PM";
  }
  response = hour+" "+minute+" "+timing;
} catch (error) {
  console.log("Exception:"+error);
}
  return response;
},
}
