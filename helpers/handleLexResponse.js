var helpermethods = require('../helpers/helpermethods');
var currencyFormatter = require('currency-formatter');

module.exports = {
  handleCustomResponse: function(intentRequest, callback, type, textmsg) {
    var msg = "Welcome Message";
    var cardtitle = "Welcome";
    var htmlmsg = "htmlmsg";
    var textaction = "textaction";
    var buttontype = "buttontype";
    var valuetype = "valuetype";
    var imageuri = "imageuri";
    try {
      var text = ["Vehicle Details", "Make an Appointment", "Dealership Details"];
      var buttontype = ["button", "button", "button"];
      var valuetype = ["Vehicle Details", "Make an Appointment", "Dealership Details"];
      var action = this.buildAction(text, valuetype);
      msg = "{\"type\": \"" + type + "\", " +
        "\"data\": [{\"imageuri\":\"" + imageuri + "\" ,\"title\": \"" +
        cardtitle + "\", \"text\": \"" + textmsg + "\"," +
        " \"action\" : " + action +
        "}]}"
    } catch (error) {
      console.log(error);
      msg = "Got in excpetion handling";
      console.log("Failed to get response from server");
    }
    console.log("Message is:" + msg);
    callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
      contentType: 'PlainText',
      content: msg
    }));

  },

  buildAction: function(text, valuetype) {
    var action = "[";
    for (var i = 0; i < text.length; i++) {
      action += "{\"text\": \"" + text[i] + "\",";
      action += "\"type\": \"button\",";
      action += "\"value\": \"" + valuetype[i] + "\"";
      if (i < text.length - 1) {
        action += "},";
      } else {
        action += "}";
      }
    }
    action += "]";
    return action;
  },

  handleVehicleResponse: function(intentRequest, callback, stringResult) {
    console.log("Result is:" + stringResult);
    var msg = "Welcome Message";
    var type = "convo";
    try {
      var data = this.buildData(stringResult);
      msg = "{\"type\": \"" + type + "\", " +
        "\"data\":" + data +
        "}"
    } catch (error) {
      console.log(error);
      msg = "Failed to get response from server";
      console.log("Got in excpetion handling");
    }
    console.log("Message is:" + msg);
    callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
      contentType: 'PlainText',
      content: msg
    }));
  },

  handleTextResponsehtml: function(intentRequest, callback, slot, message) {
    const slots = intentRequest.currentIntent.slots;
    console.log("handleTextResponsehtml started");
    var type = "elicitSlot";
    var msg = "{\"type\": \"" + type + "\", " +
      "\"data\": [{\"text\": \"" + message + "\"}]}"
    console.log(slot);
    callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, slot, {
      contentType: 'PlainText',
      content: msg
    }));
    console.log("handleTextResponsehtml completed");

  },
  handleTextResponseWithSlot: function(intentRequest, callback, slot, message) {
    const slots = intentRequest.currentIntent.slots;
    console.log("handleTextResponsehtml started");
    var type = "text";
    var msg = "{\"type\": \"" + type + "\", " +
      "\"data\": [{\"text\": \"" + message + "\"}]}"
    console.log(slot);
    callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, slot, {
      contentType: 'PlainText',
      content: msg
    }));
    console.log("handleTextResponsehtml completed");

  },

  handleTextResponse: function(intentRequest, callback, message) {
    const slots = intentRequest.currentIntent.slots;
    console.log("handleTextResponsehtml started");
    var type = "text";
    var msg = "{\"type\": \"" + type + "\", " +
      "\"data\": [{\"text\": \"" + message + "\"}]}"
    callback(helpermethods.close(intentRequest.sessionAttributes, 'Fulfilled', {
      contentType: 'PlainText',
      content: msg
    }));
    console.log("handleTextResponsehtml completed");

  },

  handleTextResponseWithAction: function(intentRequest, callback, slot, message, actionArray) {
    const slots = intentRequest.currentIntent.slots;
    console.log("handleTextResponsehtml started");
    var action = this.buildAction(actionArray, actionArray);
    var type = "text";
    var msg = "{\"type\": \"" + type + "\", " +
      "\"data\": [{\"text\": \"" + message + "\"," +
      " \"action\" : " + action +
    "}]}"
    console.log(slot);
    callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, slot, {
      contentType: 'PlainText',
      content: msg
    }));
    console.log("handleTextResponsehtml completed");

  },

  handleVehicleResponseSession: function(intentRequest, callback) {
    const slots = intentRequest.currentIntent.slots;
    var msg = "Do you have any more details for the car that you are looking for?.";
    console.log("handleVehicleResponseSession started");
    var type = "text";
    msg = "{\"type\": \"" + type + "\", " +
      "\"data\": [{\"text\": \"" + msg + "\"}]}"
    const yesorno = intentRequest.currentIntent.slots.yesorno;
    console.log(yesorno);
    callback(helpermethods.elicitSlot(intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots, "yesorno", {
      contentType: 'PlainText',
      content: msg
    }));
    console.log("handleVehicleResponseSession completed");
  },

  buildData: function(stringResult) {
    var totalNo = 15;
    if (stringResult.length < totalNo) {
      totalNo = stringResult.length;
    }
    var data = "[";
    for (var i = 0; i < totalNo; i++) {
      data += "{\"imageuri\": \"" + stringResult[i].thumbnailLarge + "\",";
      data += "\"title\": \"" + stringResult[i].trim + "\",";
      var currency = currencyFormatter.format(stringResult[i].msrp, {
        code: 'USD'
      });
      data += "\"text\": \"MSRP - " + currency + "\"";
      if (i < totalNo - 1) {
        data += "},";
      } else {
        data += "}";
      }
    }
    data += "]";
    return data;
  },

}
