module.exports = {
  parseLocalDate: function (date) {
    const dateComponents = date.split(/\-/);
    return new Date(dateComponents[0], dateComponents[1] - 1, dateComponents[2]);
  },
  isValidDate: function (date) {
    console.log("isvaliddate method")
    try {
      return !(isNaN(parseLocalDate(date).getTime()));
    } catch (err) {
      return false;
    }
  }
}
