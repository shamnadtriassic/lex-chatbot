'use strict';
var lexmethod = require('./helpers/lexmethods');
// Route the incoming request based on intent.
// The JSON body of the request is provided in the event slot.
exports.handler = (event, context, callback) => {
  try {
    // By default, treat the user request as coming from the America/New_York time zone.
    process.env.TZ = 'America/New_York';
    /*
    if (event.bot.name !== 'OrderFlowers') {
         callback('Invalid Bot Name');
    }
    */
    console.log("Event is"+event.name);
    lexmethod.dispatch(event, (response) => callback(null, response));
  } catch (err) {
    callback(err);
  }
};
